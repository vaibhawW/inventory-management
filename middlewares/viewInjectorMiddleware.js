const url = require('url')
module.exports = function(req,res,next)
{
	let link = url.parse(req.url).pathname;
	let routes = ['/'].concat(...link.split('/'));
	if(link==='/') {
		link = '/home';
		routes = ['/','/'];
	}
	let path = "dashboard"+link;
	res.locals.crumbs = path.split('/').map((e,i) => {
		return {
			name: e[0].toUpperCase() + e.slice(1),
			url: routes[i]
		}
	});
	next();
}
