const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const vendorSchema = new Schema({
	name: String,
	email: String
});

exports.Vendor = mongoose.model('Vendor', vendorSchema);
