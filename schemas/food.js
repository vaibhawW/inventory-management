const {composeWithMongoose} = require("graphql-compose-mongoose");
const {schemaComposer} = require('graphql-compose');
const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const {Vendor} = require('./vendor')

const inventoryBase = {
	quantity: Number,
	expiry: Date,
	purchase: Date,
	quantityOnPurchase: Number,
	vendor: {
		name: String,
		vendor_id: Schema.ObjectId
	}
};

const inventorySchema = new Schema(inventoryBase);

const checkoutSchema = new Schema({
	quantity: Number,
	slot: Number,
	date: Number,
	chef: {
		name: String,
		chef_id: Schema.ObjectId
	}
});

const foodSchema = new Schema({
	name: String,
	expiry: {type: Number, default: 0},
	quantity: {type: Number, default: 0},
	total: {type: Number, default: 0},
	inventories: [inventorySchema],
	oldInventories: [inventorySchema],
	checkouts: [checkoutSchema]
});

foodSchema.methods.addInventory = function (inventory) {
	inventory.quantityOnPurchase = inventory.quantity;
	this.inventories.push(inventory);
	this.total = (this.total !== undefined && this.total !== null ? this.total : 0) + inventory.quantity;
	this.markModified('inventory');
	return this;
};

foodSchema.statics.findPaginated = function(args)
{
	let pagination = null;
	if("pagination" in args)
	{
		pagination = args.pagination;
		delete args.pagination;
	}
	return this.find(args, null, pagination);
}

foodSchema.methods.addCheckout = function (checkout) {
	if (checkout.quantity > this.quantity)
		throw new error("Quantity checked out is more then total quantity in inventory");
	this.checkouts = this.checkouts !== undefined ? this.checkouts : [];
	this.checkouts.push(checkout);
	this.quantity -= checkout.quantity;
	let sortedItems = this.inventories.filter(c => c.quantity !== 0).sort((a, b) => a.expiry - b.expiry);
	let i = 0;
	let quantity = checkout.quantity;
	while (quantity !== 0) {
		let curr = sortedItems[i++];
		if (quantity - curr.quantity > 0) {
			quantity -= curr.quantity;
			curr.quantity = 0;
		} else {
			curr.quantity -= quantity;
			quantity = 0;
		}
	}
	let zeroInventory = this.inventories.filter(c => c.quantity === 0);
	this.oldInventories = this.oldInventories !== undefined ? this.oldInventories : [];
	this.oldInventories.concat(...zeroInventory);
	this.inventories = this.inventories.filter(c => c.quantity !== 0);
	this.markModified('checkout');
};


const Food = mongoose.model('Food', foodSchema);
exports.Food = Food;
