const {Food} = require("../schemas/food");
module.exports = {
	get: async (req,res,next) => {
		const foods = await Food.find();
		res.render('food/index',{foods})
	},
	nameGet: (req,res,next) => {
		food = foodItems[req.params.name];
		res.render('food/singleFood',{food: food, name: req.params.name});
	},
	editGet: (req,res,next) => {
		res.render('food/edit')
	},
	editPost: (req,res,next) => {
		let food = new Food(req.body);
		food.save();
		next();
	},
	addInventoryGet: async(req,res,next) => {
		res.render('food/inventory')
	},
	addInventoryPost: async (req,res,next) => {
		let food = await Food.findOne({'_id':req.body.id});
		await food.addInventory({
			quantity: req.body.quantity,
			expiry: req.body.expiry,
			purchase: req.body.purchase,
			vendor: {
				name: req.body.name,
				vendor_id: req.body.vendor_id
			}
		});
		await food.save();
		next();
	},
	addCheckoutGet: async (req,res,next) => res.render('food/checkout'),
	addCheckoutPost: async(req, res, next) => {
		let food = await Food.findOne({_id: req.body.id});
		req.body.slot = 1;
		req.date = Date.now();
		food.addCheckout({
			quantity: req.body.quantity,
			slot: 1,
			date: Date.now(),
			chef: {
				name: req.body.name,
				id: req.body.id
			}
		});
		await food.save();
		next()
	}
}
