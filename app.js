const createError = require('http-errors');
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const graphqlHTTP = require('express-graphql');
const mongo = require('mongoose');
const path = require('path');
const stylus = require('stylus');
const bodyParser = require('body-parser');
const passport = require('passport');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const apiRouter = require('./routes/api');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cors());
app.use(session({
  secret: 'a4sad6f54ads9f8wret4er65gr4nh9r8th4b',
  resave: false,
  saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

mongo.connect('mongodb://localhost:27017/inventory?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: true
}).then(c=>console.log('Connected to database')).catch(err=>console.log(err));
mongo.connection.on('error', function(err){
  console.error.bind(console, 'Connection refused')
});

app.use('/graphql', graphqlHTTP({
  schema: require('./graphql/schema'),
  rootValue: require('./graphql/resolver'),
  graphiql: true
}));
app.use(cookieParser());
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(passport.initialize({}));
app.use(passport.session({}));


app.use('/api',apiRouter);
app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
