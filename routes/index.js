const express = require('express');

const foodController =  require('../controllers/foodController')

const router = express.Router();
const viewInjector = require('../middlewares/viewInjectorMiddleware');

router.use(viewInjector);

router.get('/food',foodController.get);
router.get('/food/edit',foodController.editGet);
router.post('/food/edit',foodController.editPost);
router.get('/food/inventory', foodController.addInventoryGet);
router.post('/food/inventory', foodController.addInventoryPost);
router.get('/food/checkout',foodController.addCheckoutGet);
router.post('/food/checkout', foodController.addCheckoutPost);
router.get('/food/:name', foodController.nameGet);

router.use(function(req, res, next) {
  res.title = "test";
  res.render('index', { title: 'Express' });
});

module.exports = router;
