const mongoose = require('mongoose')
module.exports = {
	find: async (model, args) => {
		if (args.id !== undefined) {
			args._id = mongoose.Types.ObjectId(args.id);
			delete args.id;
		}
		return await model.findPaginated(args);
	},
	findOne: (model, args) => {
		if(args.id !== undefined){
			args._id = mongoose.Types.ObjectId(args.id);
			delete args.id;
		}
		return model.findOne(args);
	}
}

