const {GraphQLObjectType, GraphQLSchema, GraphQLString, GraphQLID, GraphQLInt, GraphQLFloat, GraphQLList, GraphQLNonNull, GraphQLBoolean} = require("graphql");
const foodQueries = require('./schemas/food/queries');
const foodMutations = require('./schemas/food/mutations');

const RootQuery  = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		hello: {
			type: GraphQLString,
			resolve: () => 'Query says hello world!'
		},
		...foodQueries
	}
});
const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		hello: {
			type: GraphQLString,
			resolve: () => 'mutation says hello world!'
		},
		...foodMutations
	}
});

module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: Mutation
});
