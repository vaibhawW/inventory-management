const {Food} = require('../../../schemas/food')
const base = require('./base');
const types = require('./type')
//
// const FoodParams = new GraphQLObjectType({
// 	name: 'Food',
// 	fields: {
// 		"foods": {type: new GraphQLList(FoodType)}
// 	}
// });
//
// const FoodListQuery = {
// 	name: 'Food',
// 	type: new GraphQLList(FoodParams),
// 	args: {
// 		id: {type: GraphQLID},
// 		...base.food
// 	},
// 	resolve: function (parent, args) {
// 		return Food.find(args)
// 	}
// };
//
// const FoodPostMutation = {
// 	name: 'foodPost',
// 	args: base.food,
// 	resolve: async (parent, args) => {
// 		food = new Food(args);
// 		await food.save();
// 		return food;
// 	},
// 	type: FoodParams
// };
//
// const FoodPutMutation = {
// 	name: 'foodPut',
// 	args: {
// 		id: {type: GraphQLID, required: true},
// 		...base.food
// 	},
// 	resolve: async (parent, args) => {
// 		await Food.findOneAndUpdate({'_id': args.id}, args);
// 		return Food.findOne({_id: args.id});
// 	},
// 	type: FoodParams
// };
//
// const FoodDeleteMutation = {
// 	name: 'foodDelete',
// 	args: {
// 		id: {type: GraphQLID, required: true}
// 	},
// 	resolve: async (parent, args) => {
// 		await Food.findOneAndDelete({'_id': args.id});
// 		return args.id;
// 	},
// 	type: GraphQLID
// };
//
// const FoodAddInventoryMutation = {
// 	name: 'foodAddInventory',
// 	args: {
// 		id: {type: GraphQLID, required: true},
// 		...base.food,
// 		inventories: {
// 			type: new GraphQLInputObjectType({
// 				name: 'InventoryInput',
// 				fields: {
// 					expiry: base.inventory.expiry,
// 					quantity: base.inventory.quantity
// 				}
// 			})
// 		}
// 	},
// 	resolve: async (parent, args) => {
// 	},
// 	type: GraphQLString
// }

exports.queries = {
	food: {
		name: 'Food',
		type: types.foods,
		resolve: async () => {
			return await Food.find();
		}
	}
}
// exports.Mutations = {
// 	foodPost: FoodPostMutation,
// 	foodPut: FoodPutMutation,
// 	foodDelete: FoodDeleteMutation,
// 	addInventory: FoodAddInventoryMutation
// }
