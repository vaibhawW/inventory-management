const type = require('./type')
const args = require('./args')
const {Food} = require('../../../schemas/food')
const helper = require('../../helpers/resolver');


module.exports = {
	food: {
		name: 'food',
		type: type.foods,
		args: args.food,
		resolve: async (parent, args) => {
			return await helper.find(Food, args);
		}
	}
}
