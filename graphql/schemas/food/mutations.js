const {GraphQLID, GraphQLNonNull, GraphQLInputObjectType} = require('graphql')
const {Food} = require('../../../schemas/food')

const type = require('./type')
const base = require('./base')
const args = require('./args')
const input = require('./input')
const helper = require('../../helpers/resolver');

const post = {
	name: 'post',
	type: type.food,
	args: input.food,
	resolve: async (parent, args) => {
		let food = new Food(args);
		await food.save();
		return food;
	}
}

const put = {
	name: 'put',
	type: type.food,
	args: {
		id: {type: GraphQLID, required: true},
		...input.food
	},
	resolve: async (parent, args) => {
		await Food.findOneAndUpdate({_id: args.id}, args);
		return Food.findOne({_id: args.id});
	}
}

const del = {
	name: 'delete',
	type: type.food,
	args: {
		id: {type: GraphQLID, required: true}
	},
	resolve: async (parent, args) => {
		const food = await helper.findOne(Food, args)
		await Food.findOneAndDelete(args)
		return food;
	}
}

const addInventory = {
	name: 'addInventory',
	type: type.food,
	args: {
		inventory: {type: input.inventoryType},
		food: {
			type: new GraphQLInputObjectType({
				name: "InventoryFoodId",
				fields: {
					id: {type: new GraphQLNonNull(GraphQLID)}
				}
			})
		}
	},
	resolve: (parent, args) => {
		console.log(args);
	}
}

module.exports = {
	foodPost: post,
	foodPut: put,
	foodDelete: del,
	addInventory: addInventory
}
