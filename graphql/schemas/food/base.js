const {GraphQLInt, GraphQLString} = require("graphql");
vendor = {
	name: {type: GraphQLString}
};
inventory = {
	quantity: {type: GraphQLInt},
	expiry: {type: GraphQLString},
	purchase: {type: GraphQLString}
};
oldInventory = (function () {
	return Object.assign(this.inventory);
})();

chef = {
	name: {type: GraphQLString}
};
checkout = {
	quantity: {type: GraphQLInt},
	slot: {type: GraphQLInt},
	date: {type: GraphQLString},
};
food = {
	name: {type: GraphQLString},
	quantity: {type: GraphQLInt},
	expiry: {type: GraphQLInt},
	total: {type: GraphQLInt}
};

module.exports = {
	vendor,
	chef,
	inventory,
	oldInventory,
	checkout,
	food
}
