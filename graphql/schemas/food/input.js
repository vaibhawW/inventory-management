const base = require('./base')
const {GraphQLID, GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull} = require('graphql')

const vendor = base.vendor;
const vendorType = new GraphQLInputObjectType({
	name: "VendorInput",
	fields: vendor
});

const chef = base.chef;
const chefType = new GraphQLInputObjectType({
	name: "ChefInput",
	fields: chef
});

const inventory = {
	...base.inventory,
	vendor: {type: vendorType}
};
const inventoryType = new GraphQLInputObjectType({
	name: "InventoryInput",
	fields: inventory
});

const oldInventory = base.oldInventory;
const oldInventoryType = new GraphQLInputObjectType({
	name: "OldInventoryInput",
	fields: oldInventory
});

const checkout = base.checkout;
const checkoutType = new GraphQLInputObjectType({
	name: "CheckoutInput",
	fields: checkout
});

const food = {
	...(function () {
		let inputFood = Object.assign(base.food);
		delete inputFood.total;
		return inputFood;
	})(),
	inventories: {type: inventoryType},
	oldInventories: {type: oldInventoryType},
	checkouts: {type: checkoutType}
};

const foodType = new GraphQLInputObjectType({
	name: "FoodInput",
	fields: food
});

module.exports = {
	food,
	inventory,
	inventoryType
}
