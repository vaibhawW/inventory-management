const base = require('./base')
const {GraphQLID, GraphQLInt, GraphQLInputObjectType} = require("graphql");

const pagination = {
	skip: {type: GraphQLInt},
	limit: {type: GraphQLInt}
}
const paginationType = new GraphQLInputObjectType({
	name: "Pagination",
	fields: pagination
});

const vendor = {
	id: {type: GraphQLID},
	...base.vendor
};

const vendorType = new GraphQLInputObjectType({
	name: 'VendorArgs',
	fields: vendor
});

const chef = {
	id: {type: GraphQLID},
	...base.chef
};

const chefType = new GraphQLInputObjectType({
	name: 'ChefArgs',
	fields: chef
});

const inventory = {
	id: {type: GraphQLID},
	...base.inventory
};

const inventoryType = new GraphQLInputObjectType({
	name: 'InventoryArgs',
	fields: inventory
});

const oldInventory = {
	id: {type: GraphQLID},
	...base.oldInventory
};

const oldInventoryType = new GraphQLInputObjectType({
	name: 'OldInventoryArgs',
	fields: oldInventory
});

const checkout = {
	id: {type: GraphQLID},
...base.checkout
};

const checkoutType = new GraphQLInputObjectType({
	name: 'CheckoutArgs',
	fields: checkout
});

const food = {
	...base.food,
	inventory: {type: inventoryType},
	oldInventory: {type: oldInventoryType},
	checkout: {type: checkoutType},
	pagination: {type: paginationType}
};

const foodType = new GraphQLInputObjectType({
	name: "FoodArgs",
	fields: food
});

module.exports = {
	food,
	inventory,
	foodType,
	inventoryType
}
