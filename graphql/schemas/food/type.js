const {GraphQLID, GraphQLList, GraphQLObjectType} = require("graphql");
const base = require('./base');

const vendor = new GraphQLObjectType({
	name: 'Vendor',
	fields: {
		id: {type: GraphQLID},
		...base.vendor
	}
});

const chef = new GraphQLObjectType({
	name: 'Chef',
	fields: {
		id: {type: GraphQLID},
		...base.chef
	}
});

const inventory = new GraphQLObjectType({
		name: 'Inventory',
		fields: {
			id: {type: GraphQLID},
			...base.inventory,
			vendor: {type: vendor}
		}
});

const inventories =  new GraphQLList(inventory);

const oldInventory = new GraphQLObjectType({
	name: 'OldInventory',
	fields: {
		id: {type: GraphQLID},
		...base.oldInventory,
		vendor: {type: vendor}
	}
});

const oldInventories = new GraphQLList(oldInventory);

const checkout = new GraphQLObjectType({
	name: 'Checkout',
	fields: {
		id: {type: GraphQLID},
		...base.checkout,
		chef: {type: chef}
	}
});

const checkouts = new GraphQLList(checkout);

const food = new GraphQLObjectType({
	name: 'FoodType',
	fields: {
		id: {type: GraphQLID},
		...base.food,
		inventories: {type: inventories},
		oldInventories: {type: oldInventories},
		checkouts: {type: checkouts}
	}
});

const foods = new GraphQLList(food);

module.exports = {
	vendor,
	chef,
	inventory,
	inventories,
	oldInventory,
	oldInventories,
	checkout,
	checkouts,
	food,
	foods
}
